FROM php:8.3.4-apache

# Set the working directory in the container
WORKDIR /var/www/html

# Install mysqli module to connect to mysql
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli
RUN chown -R www-data:www-data /var/www

# Expose the port Apache listens on
EXPOSE 80

# Start Apache when the container runs
CMD ["apache2-foreground"]