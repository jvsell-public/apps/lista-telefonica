<?php
// Realiza a conexão com o banco
include ('conexao.php');

// Função para gerar um nome aleatório
function gerarNomeAleatorio()
{
    $nomes = array( "José", "João", "Antônio", "Francisco", "Carlos", "Paulo", "Marcelo", "Roberto", "Rafael", "Eduardo", "Anderson", "Leonardo", "Bruno", "Diego", "Fernando", "Rodrigo", "André", "Luiz", "Márcio", "Wagner", "Thiago", "William", "Gabriel", "Marcos", "Alexandre", "Fábio", "Vinicius", "Felipe", "Danilo", "Maurício", "Matheus", "Ricardo", "Gustavo", "Pedro", "Hugo", "Cristiano", "Luciano", "Rafael", "Victor", "Alan", "Eduardo", "Adriano", "Jorge", "Douglas", "Everton", "César", "Cláudio", "Marcelo", "Jonas", "Sérgio", "Mário", "Fábio", "João", "Henrique", "Bruno", "Daniel", "Fernando", "Rafael", "Cristiano", "Leonardo", "Michel", "Rafael", "Carlos", "Marcelo", "Eduardo", "Bruno", "Thiago", "Gabriel", "Anderson", "Matheus", "João", "Pedro", "Leonardo", "Lucas", "Vinicius", "Rafael", "João", "José", "Maria", "Ana", "Francisca", "Adriana", "Jéssica", "Camila", "Roberta", "Fernanda", "Michele", "Simone", "Thais", "Caroline", "Patricia", "Vanessa", "Andréia", "Mariana", "Luana", "Amanda", "Cristiane", "Márcia", "Elaine", "Gabriela", "Daniele", "Juliana", "Iara", "Fernanda", "Ana", "Camila", "Michele", "Simone", "Thais", "Caroline", "Patricia", "Vanessa", "Andréia", "Mariana", "Luana", "Amanda", "Cristiane", "Márcia", "Elaine", "Gabriela", "Daniele", "Juliana", "Iara", "Fernanda", "Ana", "Camila", "Michele", "Simone", "Thais", "Caroline", "Patricia", "Vanessa", "Andréia", "Mariana", "Luana", "Amanda", "Cristiane", "Márcia", "Elaine", "Gabriela", "Daniele", "Juliana", "Iara", "Fernanda");
    
    $sobrenomes = array( "Silva", "Santos", "Oliveira", "Pereira", "Souza", "Ferreira", "Almeida", "Costa", "Lima", "Gomes", "Rocha", "Machado", "Rodrigues", "Carvalho", "Martins", "Nunes", "Cardoso", "Ribeiro", "Araújo", "Barbosa", "Alves", "Dias", "Sousa", "Vieira", "Mendes", "Teixeira", "Pinto", "Sales", "Campos", "Batista", "Rodrigues", "Souza", "Pereira", "Silva", "Oliveira", "Santos", "Fernandes", "Gomes", "Costa", "Martins", "Almeida", "Barbosa", "Alves", "Ferreira", "Carvalho", "Pereira", "Souza", "Silva", "Oliveira", "Santos", "Fernandes", "Gomes", "Costa", "Martins", "Almeida", "Barbosa", "Alves", "Ferreira", "Carvalho", "Pereira", "Souza", "Silva", "Oliveira", "Santos", "Fernandes", "Gomes", "Costa", "Martins", "Almeida", "Barbosa", "Alves", "Ferreira", "Carvalho", "Pereira", "Souza", "Silva", "Oliveira", "Santos", "Fernandes", "Gomes", "Costa", "Martins", "Almeida", "Barbosa", "Alves", "Ferreira", "Carvalho");
    
    $nome = $nomes[array_rand($nomes)];
    $sobrenome = $sobrenomes[array_rand($sobrenomes)];

    return $nome . " " . $sobrenome;
}

// Função para gerar um telefone aleatório
function gerarTelefoneAleatorio()
{
    // Seleciona aleatoriamente entre números de telefone fixo e celular
    $tipoTelefone = rand(0, 1); // 0 para fixo, 1 para celular
    
    // DDDs válidos no Brasil
    $ddd = array('11', '12', '13', '14', '15', '16', '17', '18', '19', '21', '22', '24', '27', '28', '31', '32', '33', '34', '35', '37', '38', '41', '42', '43', '44', '45', '46', '47', '48', '49', '51', '53', '54', '55', '61', '62', '63', '64', '65', '66', '67', '68', '69', '71', '73', '74', '75', '77', '79', '81', '82', '83', '84', '85', '86', '87', '88', '89', '91', '92', '93', '94', '95', '96', '97', '98', '99');

    // Seleciona aleatoriamente um DDD da lista de DDDs válidos
    $dddSelecionado = $ddd[array_rand($ddd)];

    if ($tipoTelefone == 0) {
        // Telefone fixo: (XX) XXXX-XXXX
        $telefone = '(' . $dddSelecionado . ') ' . rand(1000, 9999) . '-' . rand(1000, 9999);
    } else {
        // Celular: (XX) 9 XXXX-XXXX
        $telefone = '(' . $dddSelecionado . ') 9 ' . rand(1000, 9999) . '-' . rand(1000, 9999);
    }

    return $telefone;
}


// Função para gerar um email aleatório
function gerarEmailAleatorio($nome)
{
    $dominios = array("gmail.com", "hotmail.com", "yahoo.com", "outlook.com", "icloud.com", "aol.com");
    
    // Dividir o nome completo em partes
    $partesNome = explode(" ", $nome);
    $primeiroNome = $partesNome[0];
    $sobrenome = end($partesNome);

    // Montar o email com base no primeiro nome e sobrenome
    $email = strtolower($primeiroNome . "." . $sobrenome . "@" . $dominios[array_rand($dominios)]);
    
    return $email;
}

// Inserção de 20 registros de teste
for ($i = 0; $i < 50; $i++) {
    $nome = gerarNomeAleatorio();
    $telefone = gerarTelefoneAleatorio();
    $email = gerarEmailAleatorio($nome);

    // Query SQL para verificar se o registro já existe
    $sqlVerificar = "SELECT * FROM usuarios WHERE nome='$nome' AND telefone='$telefone' AND email='$email'";
    $resultado = mysqli_query($db, $sqlVerificar);

    if (mysqli_num_rows($resultado) == 0) {
        // Query SQL para inserir os dados na tabela
        $sql = "INSERT INTO usuarios (nome, telefone, email) VALUES ('$nome', '$telefone', '$email')";

        // Executa a query
        if (!mysqli_query($db, $sql)) {
            echo "Erro ao adicionar registro: " . mysqli_error($db);
        }
    }
}

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Popular Cadastros</title>
    <style>
        #mensagem {
            font-size: 24px;
            font-weight: bold;
            color: white;
        }

        body {
            background-color: #0F2E5D;
        }
    </style>
</head>

<body>
    <script>
        function redirecionar() {
            var segundos = 10;
            var intervalo = setInterval(function () {
                document.getElementById("mensagem").innerHTML = "Populate concluído, redirecionando para a página inicial em " + segundos + " segundos.";
                segundos--;
                if (segundos < 0) {
                    clearInterval(intervalo);
                    window.location.href = "index.php";
                }
            }, 1000);
        }

        redirecionar();
    </script>

    <h3 id="mensagem" style="text-align: center; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"></h3>
</body>

</html>