<?php
// Recebe o ID do usuário
$id = $_GET['id'];

// Conecta ao banco de dados (inclui conexao.php)
include('conexao.php');

// Exclui o usuário da tabela
$sql = "DELETE FROM usuarios WHERE id = ?";
$stmt = mysqli_prepare($db, $sql);
mysqli_stmt_bind_param($stmt, "i", $id);
mysqli_stmt_execute($stmt);
mysqli_stmt_close($stmt);

// Redireciona para a página inicial
header('Location: index.php');
?>
