<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista Telefônica</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="styles/styles.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">
</head>

<body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/jquery.inputmask.min.js"></script>

    <?php
    // Realiza a conexão com o banco e checagem da necessidade de setup
    include ('conexao.php');

    // Defina o número de itens por página
    $itemsPorPagina = 10;

    // Determine a página atual
    $paginaAtual = isset($_GET['pagina']) ? $_GET['pagina'] : 1;

    // Calcule o offset para a consulta SQL
    $offset = ($paginaAtual - 1) * $itemsPorPagina;

    // Consulta SQL com paginação
    $sql = "SELECT * FROM usuarios ORDER BY id DESC LIMIT $offset, $itemsPorPagina";
    $result = mysqli_query($db, $sql);

    // Calcular o total de páginas
    $totalUsuarios = mysqli_num_rows(mysqli_query($db, "SELECT * FROM usuarios"));
    $totalPaginas = ceil($totalUsuarios / $itemsPorPagina);
    ?>

    <div class="header">
        <div class="title">LISTA DE CONTATOS</div>
    </div>

    <?php
    echo '<div class="container_actions">';
    echo '  <div class="form_add_user">';
    echo '      <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal"><i class="fa fa-user-plus"></i>ﾠAdicionar</button>';
    echo '  </div>';
    echo '</div>';

    echo '<div class="modal fade" id="modal" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">';
    echo '    <div class="modal-dialog">';
    echo '        <div class="modal-content">';
    echo '            <div class="modal-header">';
    echo '                <h5 class="modal-title" id="modalLabel">Adicionar Contato</h5>';
    echo '                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>';
    echo '            </div>';
    echo '            <div class="modal-body">';
    echo '                <form id="form" action="processar_formulario.php" method="post">';
    echo '                    <input type="text" id="nome" maxlength="40" name="nome" placeholder="Nome" class="form-control" required>';
    echo '                    <input type="tel" id="telefone" maxlength="19" name="telefone" placeholder="Telefone" class="form-control" required>';
    echo '                    <input type="email" id="email" maxlength="40" name="email" placeholder="Email" class="form-control" required>';
    echo '                    <input type="submit" value="Salvar" class="btn btn-primary">';
    echo '                </form>';
    echo '            </div>';
    echo '        </div>';
    echo '    </div>';
    echo '</div>';
    ?>

    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">Telefone</th>
                <th scope="col">Email</th>
                <th scope="col">Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $result = mysqli_query($db, $sql);
            while ($row = mysqli_fetch_assoc($result)) {
                echo "<tr>";
                echo "<td>" . $row['nome'] . "</td>";
                echo "<td>" . $row['telefone'] . "</td>";
                echo "<td>" . $row['email'] . "</td>";
                echo "<td>";
                echo "<a href='#' data-bs-toggle='modal' data-bs-target='#modaledit" . $row['id'] . "'><i class='fa fa-edit'></i></a> | ";
                echo "<a href='excluir.php?id=" . $row['id'] . "'><i class='fa fa-trash'></i></a>";
                echo "</td>";
                echo "</tr>";

                // Define a modal do botão de edição de contato
                echo '<div class="modal fade" id="modaledit' . $row['id'] . '" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">';
                echo '    <div class="modal-dialog">';
                echo '        <div class="modal-content">';
                echo '            <div class="modal-header">';
                echo '                <h5 class="modal-title" id="modalLabel">Edição de Contato</h5>';
                echo '                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>';
                echo '            </div>';
                echo '            <div class="modal-body">';
                echo '                <form id="form_edit_' . $row['id'] . '" action="processar_editar.php" method="post" >';
                echo '                    <input type="hidden" name="id" value="' . $row['id'] . '" class="form-control">';
                echo '                    <input type="text" id="nome_edit_' . $row['id'] . '" maxlength="40" name="nome" value="' . $row['nome'] . '" placeholder="Nome" class="form-control" required>';
                echo '                    <input type="tel" id="telefone_edit_' . $row['id'] . '" maxlength="19" name="telefone" value="' . $row['telefone'] . '" placeholder="Telefone" class="telefone-edit form-control" required>';
                echo '                    <input type="email" id="email_edit_' . $row['id'] . '" maxlength="40" name="email" value="' . $row['email'] . '" placeholder="Email" class="form-control" required>';
                echo '                    <input type="submit" value="Salvar" class="btn btn-primary">';
                echo '                </form>';
                echo '            </div>';
                echo '        </div>';
                echo '    </div>';
                echo '</div>';
            }
            ?>
        </tbody>
    </table>

    <!-- Adicione controles de paginação -->
    <div class="pagination-container text-center mt-5 d-flex justify-content-center" style="margin: 0 auto;">
        <nav aria-label="Page navigation">
            <ul class="pagination">
                <?php
                $maxLinks = 10; // Número máximo de links de página a serem exibidos
                $inicio = max(1, $paginaAtual - floor($maxLinks / 2));
                $fim = min($totalPaginas, $inicio + $maxLinks - 1);

                // Garante que o início seja ajustado corretamente quando o fim for máximo
                $inicio = max(1, $fim - $maxLinks + 1);

                // Exibe link para a página anterior
                if ($paginaAtual > 1) {
                    echo '<li class="page-item"><a class="page-link" href="?pagina=' . ($paginaAtual - 1) . '">Anterior</a></li>';
                }

                // Mostra '...' para retroceder se houver páginas anteriores não exibidas
                if ($inicio > 1) {
                    $paginaRetroceder = max(1, ceil(($inicio - $maxLinks) / 10) * 10 + 5);
                    echo '<li class="page-item"><a class="page-link" href="?pagina=' . $paginaRetroceder . '">...</a></li>';
                }

                // Exibe links de página
                for ($i = $inicio; $i <= $fim; $i++) {
                    if ($i == $paginaAtual) {
                        echo '<li class="page-item active"><a class="page-link" href="#">' . $i . '</a></li>';
                    } else {
                        echo '<li class="page-item"><a class="page-link" href="?pagina=' . $i . '">' . $i . '</a></li>';
                    }
                }

                // Mostra '...' para avançar se houver páginas seguintes não exibidas
                if ($fim < $totalPaginas) {
                    $proximaMultiplo10 = ceil($paginaAtual / 10) * 10 + 5;
                    echo '<li class="page-item"><a class="page-link" href="?pagina=' . $proximaMultiplo10 . '">...</a></li>';
                }

                // Exibe link para a próxima página
                if ($paginaAtual < $totalPaginas) {
                    echo '<li class="page-item"><a class="page-link" href="?pagina=' . ($paginaAtual + 1) . '">Próxima</a></li>';
                }
                ?>
            </ul>
        </nav>
    </div>

    <!-- INICIO DO FOOTER -->
    <?php
    // Obter o nome do host
    $hostName = getenv('HOSTNAME');

    // Obter o endereço IPv4 do conteiner
    $ip = gethostbyname($hostName);

    ?>

    <div class="footer_container">
        <div class="footer">
            <div class="footer-text">
                Está aplicação está rodando em
                <b>
                    <?php echo $hostName; ?>
                </b>
            </div>
            <div class="footer-version">v0.7.0</div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            // Máscara para o modal de adicionar usuário
            $('#telefone').inputmask({
                mask: ['(99) 9999-9999', '(99) 9 9999-9999'],
                keepStatic: true
            });

            // Máscara para o modal de edição de usuário
            $('.telefone-edit').inputmask({
                mask: ['(99) 9999-9999', '(99) 9 9999-9999'],
                keepStatic: true
            });
        });
    </script>
</body>

</html>