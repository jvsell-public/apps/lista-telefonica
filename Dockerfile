FROM php:8.3.4-apache

# Set working directory
WORKDIR /var/www/html

# Copy application
COPY ./app .

# Install MySQLi extension
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli
RUN chown -R www-data:www-data /var/www

# Expose port where Apache still listen
EXPOSE 80

# Start apache when the container runs
CMD ["apache2-foreground"]